#!/bin/bash

while ! ip link show eth1 > /dev/null 2>&1; do
    echo "Waiting for eth1 to be created..."
    sleep 1
done

ip addr flush dev eth1
ip link set eth1 promisc on


envsubst < /opt/arkime/etc/config.ini.sample  > /opt/arkime/etc/config.ini

echo "INIT" | /opt/arkime/db/db.pl --prefix ${CLIENT_NAME}-arkime --insecure ${ARKIME_ELASTICSEARCH} init

mkdir /opt/arkime/logs

(
    ulimit -c unlimited
    ulimit -l unlimited
    /opt/arkime/bin/capture --insecure -c /opt/arkime/etc/config.ini >> /opt/arkime/logs/capture.log 2>> /opt/arkime/logs/capture-errors.log &
)

#/opt/arkime/bin/capture -c /opt/arkime/etc/config.ini >> /opt/arkime/logs/capture.log 2>&1 &
#/opt/arkime/bin/capture --insecure -c /opt/arkime/etc/config.ini &

cd /opt/arkime/viewer
/opt/arkime/bin/node addUser.js --insecure -c /opt/arkime/etc/config.ini admin Admin admin --admin
/opt/arkime/bin/node viewer.js --insecure -c /opt/arkime/etc/config.ini >> /opt/arkime/logs/viewer.log 2>> /opt/arkime/logs/viewer-errors.log &

cd /

exec "$@"