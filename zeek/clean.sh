#!/bin/bash

# Solicitar al usuario que ingrese el número de VLAN
read -p "Ingrese el número de VLAN: " vlan

# Detener y eliminar el contenedor Docker con el nombre correspondiente a la VLAN
echo "Deteniendo y eliminando el contenedor zeek-vlan${vlan}."
if docker ps -a | grep -q "zeek-vlan${vlan}"; then
  docker stop zeek-vlan${vlan}
  docker rm zeek-vlan${vlan}
else
  echo "El contenedor zeek-vlan${vlan} no existe."
fi

# Verificar si la red Docker existe y eliminarla
echo "Eliminando el bridge de monitorización ids-monitor-${vlan}."
if docker network ls | grep -q "ids-monitor-${vlan}"; then
  docker network rm ids-monitor-${vlan}
else
  echo "El bridge ids-monitor-${vlan} no existe."
fi

# Verificar si la configuración de tc qdisc existe y eliminarla
echo "Eliminando el mirro desde monitoring1.${vlan}."
if tc qdisc show dev monitoring1.${vlan} | grep -q "ingress ffff"; then
  tc qdisc del dev monitoring1.${vlan} ingress
else
  echo "La configuración de tc qdisc para monitoring1.${vlan} no existe."
fi

echo "Script de limpieza completado."
