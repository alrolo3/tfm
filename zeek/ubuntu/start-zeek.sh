#!/bin/bash

while ! ip link show eth1 > /dev/null 2>&1; do
    echo "Waiting for eth1 to be created..."
    sleep 1
done

ip addr flush dev eth1
ip link set eth1 promisc on

/opt/zeek/bin/zeekctl deploy

exec "$@"