#!/bin/bash

# Solicitar al usuario que ingrese el número de VLAN
read -p "Ingrese el número de VLAN: " vlan

# Verificar si la red Docker ya existe

echo "Creando el bridge de monitorización ids-monitor-${vlan}."
if ! docker network ls | grep -q "ids-monitor-${vlan}"; then
  # Crear la red Docker con el VLAN proporcionado
  docker network create ids-monitor-${vlan}
else
  echo "El bridge ids-monitor-${vlan} ya existe."
fi

# Ejecutar el contenedor Docker con el nombre correspondiente a la VLAN

echo "Lanzando el contenedor zeek-vlan${vlan}."
docker run --privileged --cap-add=NET_ADMIN --cap-add=NET_RAW --cap-add=SYS_NICE --name zeek-vlan${vlan} -d \
-e ELASTIC_HOST="163.117.141.22" -e ELASTIC_USERNAME="elastic" \
-e ELASTIC_PASSWORD="oPhFwAnktiY7Rei222YV" -e CONTAINER_NAME="zeek-vlan${vlan}" zeek-ubuntu-1.2

# Conectar el contenedor a la red Docker creada
echo "Conectando el contenedor zeek-vlan${vlan} al bridge de monitorización ids-monitor-${vlan}."
docker network connect ids-monitor-${vlan} zeek-vlan${vlan}

# Obtener la nueva interfaz creada por el contenedor en el host
iflink=$(docker exec -it zeek-vlan${vlan} cat /sys/class/net/eth1/iflink | tr -d '\r')
interfacecreated=$(ip link | grep "^${iflink}:" | awk -F': ' '{print $2}' | cut -d'@' -f1)

# Verificar si la configuración de tc qdisc ya existe
echo "Creando el mirror desde monitoring1.${vlan} hasta ${interfacecreated}."
if ! tc qdisc show dev monitoring1.${vlan} | grep -q "ingress ffff"; then
  # Configurar el tráfico para la interfaz VLAN
  tc qdisc add dev monitoring1.${vlan} handle ffff: ingress
else
  echo "La configuración de tc qdisc para monitoring1.${vlan} ya existe."
fi

tc filter add dev monitoring1.${vlan} parent ffff: protocol all u32 match u32 0 0 action mirred egress mirror dev ${interfacecreated}

