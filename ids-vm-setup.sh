#!/bin/bash

echo "Cambiando nombre ens18 a monitoring1"

ip link set ens18 down
ip link set ens18 name monitoring1
ip link set monitoring1 up

echo "Nombre cambiado !"

# Interface base
BASE_IFACE="monitoring1"

echo "Limpiando interfaces"
for VLAN in {1..10}
do
    echo "Borrando ${BASE_IFACE}.${VLAN}"
    ip link delete ${BASE_IFACE}.${VLAN}
done

echo "Creando interfaces VLAN"

for VLAN in {1..10}
do
    echo "Creando ${BASE_IFACE}.${VLAN}"
    ip link add link $BASE_IFACE name ${BASE_IFACE}.${VLAN} type vlan id $VLAN
    ip link set ${BASE_IFACE}.${VLAN} promisc on
    ip link set ${BASE_IFACE}.${VLAN} up
done

echo "VLAN interfaces from 1 to 16 have been created and configured."

