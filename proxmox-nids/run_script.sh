#!/bin/bash

#!/bin/bash

IP_REMOTA=$1
IDS=$2
NOMBRE_CLIENTE=$3
NUMERO_VXLAN_VLAN=$4

echo "IP remota: $IP_REMOTA"
echo "IDS seleccionados: $IDS"
echo "Nombre del cliente: $NOMBRE_CLIENTE"
echo "Número VXLAN/VLAN: $NUMERO_VXLAN_VLAN"



ovs-vsctl add-port vmbr1 vxlan${NUMERO_VXLAN_VLAN} -- set interface vxlan${NUMERO_VXLAN_VLAN} \
type=vxlan options:remote_ip=${IP_REMOTA} options:key=${NUMERO_VXLAN_VLAN} options:psk=${NOMBRE_CLIENTE}

ovs-ofctl add-flow vmbr1 \
"in_port=$(ovs-vsctl get Interface vxlan${NUMERO_VXLAN_VLAN} ofport),\
dl_vlan=${NUMERO_VXLAN_VLAN},actions=output:$(ovs-vsctl get Interface tap102i0 ofport)"


curl -X POST http://163.117.141.14:8080/setup \
    -H "Content-Type: application/json" \
    -d "{\"client\":\"${NOMBRE_CLIENTE}\", \"vlan\":\"${NUMERO_VXLAN_VLAN}\", \"ids\":\"${IDS}\"}"