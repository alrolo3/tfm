#! /bin/sh
ovs-ofctl add-flow vmbr1 "in_port=$(ovs-vsctl get Interface vxlan1 ofport),vlan_tci=0x1000/0x1f80,actions=output:$(ovs-vsctl get Interface tap102i0 ofport)"
ovs-ofctl add-flow vmbr1 "in_port=$(ovs-vsctl get Interface vxlan2 ofport),vlan_tci=0x1000/0x1f80,actions=output:$(ovs-vsctl get Interface tap102i0 ofport)"

