#!/bin/bash

# Comprobar que se ha proporcionado un nombre de cliente
if [ -z "$1" ]; then
  echo "Uso: $0 <nombre_cliente>"
  exit 1
fi

# Variables
NOMBRE_CLIENTE=$1
ELASTICSEARCH_URL="https://163.117.141.22:9200"
ELASTICSEARCH_USER="elastic"
ELASTICSEARCH_PASS="oPhFwAnktiY7Rei222YV" # Cambia esta contraseña según sea necesario
DEFAULT_PASS="default"

# Crear rol para el cliente
curl --insecure -u $ELASTICSEARCH_USER:$ELASTICSEARCH_PASS -X PUT "$ELASTICSEARCH_URL/_security/role/${NOMBRE_CLIENTE}_role" -H 'Content-Type: application/json' -d"
{
  \"cluster\": [\"monitor\", \"manage_index_templates\", \"read_ilm\", \"manage_ilm\", \"manage_pipeline\"],
  \"indices\": [
    {
      \"names\": [ \"${NOMBRE_CLIENTE}-*\", \"filebeat-*\" ],
      \"privileges\": [ \"all\" ]
    }
  ],
  \"applications\": [
    {
      \"application\": \"kibana-.kibana\",
      \"privileges\": [ \"all\" ],
      \"resources\": [ \"*\" ]
    }
  ]
}
"


# Crear usuario para el cliente y asignar el rol
curl --insecure -u $ELASTICSEARCH_USER:$ELASTICSEARCH_PASS -X POST "$ELASTICSEARCH_URL/_security/user/${NOMBRE_CLIENTE}" -H 'Content-Type: application/json' -d"
{
  \"password\" : \"$DEFAULT_PASS\",
  \"roles\" : [ \"${NOMBRE_CLIENTE}_role\" ],
  \"full_name\" : \"${NOMBRE_CLIENTE}\",
  \"email\" : \"${NOMBRE_CLIENTE}@example.com\"
}
"

echo "Rol y usuario creados para el cliente: $NOMBRE_CLIENTE"
