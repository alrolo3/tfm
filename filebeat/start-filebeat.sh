#!/bin/bash

envsubst < /etc/filebeat/filebeat.yml.template > /etc/filebeat/filebeat.yml

filebeat modules enable zeek suricata
filebeat setup -e
service filebeat start

exec "$@"