#!/bin/bash


# Comprueba si el archivo .env del cliente especificado existe
if [ -f ".env.${1}" ]; then
    # Exporta cada línea como una variable de entorno
    sed -i 's/\r$//' ".env.${1}"
    export $(grep -v '^#' ".env.${1}" | xargs)
else
    echo ".env.${1} file not found"
    exit 1
fi

echo "ELASTIC_HOST is $ELASTIC_HOST"
echo "ELASTIC_USERNAME is $ELASTIC_USERNAME"
echo "ELASTIC_PASSWORD is $ELASTIC_PASSWORD"
echo "KIBANA_HOST is $KIBANA_HOST"
echo "ELASTIC_CA_FINGERPRINT is $ELASTIC_CA_FINGERPRINT"
echo "VLAN is $VLAN"
echo "CLIENT_NAME is $CLIENT_NAME"

envsubst < docker-compose.yml.template > docker-compose.yml

docker compose --env-file ".env.${1}" -p ${CLIENT_NAME} up -d


all_running=false
while [ "$all_running" = false ]; do
    echo "Esperando a que todos los contenedores estén funcionando..."
    running_containers=$(docker ps --filter "name=${CLIENT_NAME}" --filter "status=running" --format "{{.ID}}" | wc -l)
    total_containers=$(docker compose -p ${CLIENT_NAME} ps -q | wc -l)
    if [ "$running_containers" -eq "$total_containers" ]; then
        all_running=true
    else
        sleep 2
    fi
done
echo "Todos los contenedores están en funcionamiento."

#Seleccionar bridge
network_id=$(docker network ls | grep "${CLIENT_NAME}_ids-monitor" | awk '{print $1}')

# Verificar si la configuración de tc qdisc ya existe
echo "Creando el mirror desde monitoring1.${VLAN} hasta el bridge de ${CLIENT_NAME} (br-${network_id})."
if ! tc qdisc show dev monitoring1.${VLAN} | grep -q "ingress ffff"; then
  # Configurar el tráfico para la interfaz VLAN
  tc qdisc add dev monitoring1.${VLAN} handle ffff: ingress
else
  echo "La configuración de tc qdisc para monitoring1.${VLAN} ya existe."
fi

tc filter add dev monitoring1.${VLAN} parent ffff: protocol all u32 match u32 0 0 action mirred egress mirror dev br-${network_id}

echo "Estableciendo la interfaz de monitorización (eth1) de los contenedores en modo promisc"

containers=$(docker network inspect -f '{{range .Containers}}{{.Name}} {{end}}' $network_id)

# Aplicar los comandos a las interfaces de los contenedores
for container in $containers; do
    iflink=$(docker exec -it $container cat /sys/class/net/eth1/iflink | tr -d '\r\n')
    interfacecreated=$(ip link | grep "^${iflink}:" | awk -F': ' '{print $2}' | cut -d'@' -f1)
    echo "Configurando interfaz ${iflink}:${interfacecreated} en el host para el contenedor: $container"
    ip addr flush dev $interfacecreated
    ip link set $interfacecreated promisc on
done