#!/bin/bash

suricata-update update-sources
suricata-update --etopen

while ! ip link show eth1 > /dev/null 2>&1; do
    echo "Waiting for eth1 to be created..."
    sleep 1
done

ip addr flush dev eth1
ip link set eth1 promisc on

suricata -c /etc/suricata/suricata.yaml --af-packet=eth1

exec "$@"