sudo apt-get install strongswan


ip route del default via 163.117.141.2 dev eth1
ip route add default via 163.117.141.2 dev eth0

ip link add vxlan1 type vxlan id 200 local 163.117.141.31 remote 163.117.141.126 dev eth1 dstport 4789
ip link add link vxlan1 name vxlan1.5 type vlan id 5

ip route add 163.117.141.126/32 via 163.117.141.2 dev eth1

ip link set vxlan1 up
ip link set vxlan1.5 up


tc qdisc add dev eth0 handle ffff: ingress
tc filter add dev eth0 parent ffff: protocol all u32 match u32 0 0 action mirred egress mirror dev vxlan1.5

systemctl restart strongswan-starter






root@zeek-manager:/home/admin# cat /etc/ipsec.secrets
%any 163.117.141.126 : PSK "swordfish"


root@zeek-manager:/home/admin# cat /etc/ipsec.conf
config setup
    uniqueids=yes

conn %default
    keyingtries=%forever
    type=transport
    keyexchange=ikev2
    auto=route
    ike=aes256gcm16-sha256-modp2048
    esp=aes256gcm16-modp2048

conn vxlan1-in-1
    left=%any
    right=163.117.141.126
    authby=psk
    leftprotoport=udp/4789
    rightprotoport=udp

conn vxlan1-out-1
    left=%any
    right=163.117.141.126
    authby=psk
    leftprotoport=udp
    rightprotoport=udp/4789