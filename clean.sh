#!/bin/bash

echo "Eliminando el mirro desde monitoring1.${vlan}."
if tc qdisc show dev monitoring1.${vlan} | grep -q "ingress ffff"; then
  tc qdisc del dev monitoring1.${vlan} ingress
else
  echo "La configuración de tc qdisc para monitoring1.${vlan} no existe."
fi